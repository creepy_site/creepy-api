import express from 'express';
import { Types } from 'mongoose';
import { Request, Response } from 'oauth2-server';

import oauth from './oauth-server';
import auth from './authenticate';
import logger from '../../server/logger';
import { User } from '../../model/oauth';

const { ObjectId } = Types;

const router = express.Router();
const OAUTH_API = '/oauth';

const options = {
  accessTokenLifetime: 60 * 60 * 24, // 2 hours
};

/*
 * GET /api/oauth/token
 * Get token
 */
router.route(`${OAUTH_API}/token`).all((req, res) => {
  const request = new Request(req);
  const response = new Response(res);

  oauth
    .token(request, response, options)
    .then((token: any) => {
      // TODO: remove unnecessary values in response
      const {
        accessToken,
        accessTokenExpiresAt,
        refreshToken,
        refreshTokenExpiresAt,
      } = token;

      return res.json({
        accessToken,
        accessTokenExpiresAt,
        refreshToken,
        refreshTokenExpiresAt,
        scope: token.scope,
      });
    }).catch((err: any) => res.status(err.code).json(err));
});

router.route(`${OAUTH_API}/me`).get(auth(), async (req, res) => {
  const { user } = req;

  // const storiesList = await getStoriesByUser(user.User._id);
  // const rating = likes.reduce((acc, item) => acc + item.count, 0);
  console.log(user);
  res.json({
    access_token: user.access_token,
    expires: user.expires,
    user: user.User.username,
    data: {
      firstPubDate: user.User.firstPubDate,
      dateRegister: user.User.dateRegister,
      accountImage: user.User.accountImage,
      // rating,
    },
  });
});

router.route(`${OAUTH_API}/register`).post(auth({ scope: ['site'] }), async (req, res) => {
  const { body } = req;
  logger.log('Register new user:', body.login);

  /* Find user */
  const findUserByLogin = (login: any) => new Promise((resolve) => {
    User.findOne({
      username: login,
    }).then(user => resolve(user));
  });

  const findUserByEmail = (email: any) => new Promise((resolve) => {
    User.findOne({
      email,
    }).then(user => resolve(user));
  });

  const user = await findUserByLogin(body.login);
  if (user) {
    return res.status(400).send({
      error: true,
      msg: 'login_already_exists',
    });
  }

  const userByEmail = await findUserByEmail(body.email);
  if (userByEmail) {
    return res.status(400).send({
      error: true,
      msg: 'email_already_exists',
    });
  }

  User.create({
    username: body.login,
    password: body.password,
    email: body.email,
    // append user scope
    scope: [ObjectId('5a5bb44ce9a1142a648d444d')],
  }).then(newUser => res.status(200).send(newUser));
});

export default router;
