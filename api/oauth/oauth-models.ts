import { Types } from 'mongoose';
import {
  OAuthRefreshToken,
  OAuthAccessToken,
  OAuthClient,
  User,
  OAuthScope,
} from '../../model/oauth';

import logger from '../../server/logger';

const { ObjectId } = Types;

export async function getAccessToken(bearerToken: any) {
  logger.trace('getAccessToken', bearerToken);
  const accessToken: any = await OAuthAccessToken
    .findOne({ access_token: bearerToken })
    .populate('User')
    .populate('OAuthClient')
    .catch(err => logger.error(`getAccessToken - Error: ${err}`));

  if (!accessToken) return false;
  const token: any = accessToken;
  /* TODO: use lean */
  logger.warn('USER', token);
  if (token.User) {
    token.user = token.User.populate('OAuthScope').toJSON();
    token.accessTokenExpiresAt = token.expires;
    logger.warn('scope', token.scope);
  } else {
    token.client = token.OAuthClient;
    token.user = {};
  }
  logger.info('result token', token);
  return token;
}

export function getClient(clientID: any, clientSecret: any) {
  logger.trace('getClient', clientID, clientSecret);

  return OAuthClient
    .findOne({ client_id: clientID, client_secret: clientSecret })
    .then((client) => {
      if (!client) {
        return null;
      }
      const resultClient: any = client.toJSON();

      resultClient.id = resultClient._id.toString();
      resultClient.grants = ['password', 'refresh_token', 'client_credentials'];

      return resultClient;
    }).catch((err) => { logger.error('getClient - Err: ', err); });
}


export function getUser(username: any, password: any) {
  logger.trace('getUser', username, password);
  return User
    .findOne({ username: { $regex: new RegExp(`^${username}$`, 'i') } })
    .populate({
      path: 'scope',
      model: 'OAuthScope',
      select: '-_id -is_default',
    })
    .lean(true)
    .then((user: any) => {
      if (!user) {
        return false;
      }
      user.scope = user.scope.map((item: any) => item.scope);
      return user.password === password ? user : false;
    })
    .catch(err => logger.error(`getUser - Err: ${err}`));
}

export function revokeToken(token: any) {
  logger.trace('revokeToken', token);
  return OAuthRefreshToken.findOne({
    where: {
      refresh_token: token.refreshToken,
    },
  }).then((rT: any) => {
    if (rT) rT.destroy();
    /**
     * As per the discussion we need set older date
     * revokeToken will expected return a boolean in future version
     * https://github.com/oauthjs/node-oauth2-server/pull/274
     * https://github.com/oauthjs/node-oauth2-server/issues/290
     */
    const expiredToken = token;
    expiredToken.refreshTokenExpiresAt = new Date('2015-05-28T06:59:53.000Z');
    return expiredToken;
  }).catch(err => logger.error(`revokeToken - Err: ${err}`));
}

function saveUserToken(token: any, client: any, user: any) {
  console.log('client', client);
  if (user) {
    token.scope = Array.isArray(user.scope) ? user.scope : [user.scope];
  }

  const promises: any[] = [
    OAuthAccessToken.create({
      access_token: token.accessToken,
      expires: token.accessTokenExpiresAt,
      OAuthClient: client._id,
      User: user._id,
      type: 'password',
      // User: user,
      scope: user.scope,
    })
  ];

  if (token.refreshToken) {
    promises.push(
      // no refresh token for client_credentials
      OAuthRefreshToken.create({
        refresh_token: token.refreshToken,
        expires: token.refreshTokenExpiresAt,
        OAuthClient: client._id,
        User: user._id,
        scope: user.scope,
      })
    )
  }

  return Promise.all(promises).then((resultsArray) => {
    logger.trace('Token Saved', resultsArray.length);
    // expected to return client and user, but not returning
    return Object.assign(
      {
        // client: client.client_id,
        // user: user.username,
        client,
        user,
        // access_token: token.accessToken, // proxy
        // refresh_token: token.refreshToken, // proxy
      },
      token,
    );
  })
  .catch((err: any) => {
    logger.error(`saveToken - Error: ${err}`);
    logger.error(err);
  });
}

export async function saveClientToken(token: any, client: any) {
  logger.trace(`Getting client token for ${client.client_id}`);
  const alreadySavedToken: any = await OAuthAccessToken.findOne({
    OAuthClient: client._id,
  });

  if (!alreadySavedToken) {
    logger.trace(`Can't find exists token. Record ${token} as new one`);
    await OAuthAccessToken.create({
      access_token: token,
      OAuthClient: client._id,
      type: 'client_credentials',
      expires: null,
      User: null,
      scope: null,
    });
  }

  return {
    accessToken: (alreadySavedToken ? alreadySavedToken.access_token : token),
    client: client.client_id,
    /* only for prevent oauth2-server error */
    user: {},
  };
}

export const saveToken = (token: any, client: any, user: any) => (user
  ? saveUserToken(token, client, user)
  : saveClientToken(token, client)
);


export function getRefreshToken(refreshToken: any) {
  logger.trace('getRefreshToken', refreshToken);
  if (!refreshToken || refreshToken === 'undefined') {
    return false;
  }
  // [OAuthClient, User]
  return OAuthRefreshToken
    .findOne({ refresh_token: refreshToken })
    .populate('User')
    .populate('OAuthClient')
    .then((savedRT: any) => ({
      user: savedRT ? savedRT.User : {},
      client: savedRT ? savedRT.OAuthClient : {},
      refreshTokenExpiresAt: savedRT ? new Date(savedRT.expires) : null,
      refreshToken,
      refresh_token: refreshToken,
      scope: savedRT.scope,
    }))
    .catch(err => logger.error(`getRefreshToken - Err: ${err}`));
}

const scopeToArray = (scope: any) => (!Array.isArray(scope) ? [scope] : scope);

export function verifyScope(token: any, pureScope: any) {
  const reqiuredScopes = scopeToArray(pureScope);
  logger.trace('verify scope for:', token, 'by', reqiuredScopes);
  if (reqiuredScopes.length === 0) {
    return true;
  }

  if (token.type === 'client_credentials') {
    return true;
  }

  const scopes = token.User.scope.map((item: any) => ObjectId(item));
  logger.trace('users scopes', scopes);

  return OAuthScope
    .find({ _id: { $in: scopes } })
    .then((data) => {
      let result = false;

      data.forEach((item: any) => {
        if (reqiuredScopes.includes(item.scope)) {
          result = true;
          return false;
        }
      });

      return result;
    }).catch(err => logger.error(`verify scope - Err: ${err}`));
}
