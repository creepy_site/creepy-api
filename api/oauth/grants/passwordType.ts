import { AbstractGrantType } from 'oauth2-server';

class ClientCredentialsGrantType extends AbstractGrantType {
  async handle(request, client) {
    if (!request) {
      throw new Error('Missing parameter: `request`');
    }

    if (!client) {
      throw new Error('Missing parameter: `client`');
    }

    const token = await this.generateAccessToken(client);
    return this.model.saveClientToken(token, client);
  }
}

export default ClientCredentialsGrantType;
