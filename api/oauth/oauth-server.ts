import OAuth2Server, {
  UnauthorizedRequestError,
  OAuthError,
  ServerError,
} from 'oauth2-server';
import * as model from './oauth-models';
import clientCredentials from './grants/clientCredentials';
import AuthenticateHandler from './authenticate-handle';

class OAuthServerCustom extends OAuth2Server {
  authenticate(request, response, options) {
    let resultOpts: any = {};
    if (typeof options === 'string') {
      resultOpts.scope = options;
    } else {
      resultOpts = options;
    }

    Object.assign(
      resultOpts,
      {
        addAcceptedScopesHeader: true,
        addAuthorizedScopesHeader: true,
        allowBearerTokensInQueryString: false,
      },
       // @ts-ignore
      this.options,
    );

    return new AuthenticateHandler(resultOpts)
      .handle(request, response)
      // .then(callback)
      .catch((e) => {
        // Include the "WWW-Authenticate" response header field if the client
        // lacks any authentication information.
        //
        // @see https://tools.ietf.org/html/rfc6750#section-3.1
        if (e instanceof UnauthorizedRequestError) {
          response.set('WWW-Authenticate', 'Bearer realm="Service"');
        }

        if (!(e instanceof OAuthError)) {
          throw new ServerError(e);
        }

        throw e;
      });
  }
}

export default new OAuthServerCustom({
  // @ts-ignore
  model,
  extendedGrantTypes: {
    client_credentials: clientCredentials,
  },
  debug: true,
});
