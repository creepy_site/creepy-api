import { Request, Response } from 'oauth2-server';
import oauth from './oauth-server';

export default function (...scope: any[]) {
  return (req: any, res: any, next: any) => {
    const request = new Request({
      headers: {
        authorization: req.headers.authorization,
      },
      method: req.method,
      query: req.query,
      body: req.body,
    });
    const response = new Response(res);
    oauth.authenticate(request, response, { scope })
      .then((token: any) => {
        // Request is authorized.
        req.user = token;
        next();
      })
      .catch((err: any) => {
        /* There is no found user */
        if (scope.length === 0) {
          return next();
        }
        // Request is not authorized.
        res.status(err.code || 500).json(err);
      });
  };
}
