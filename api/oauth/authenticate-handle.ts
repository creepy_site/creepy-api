import { InvalidTokenError } from 'oauth2-server';
import BaseHandler from 'oauth2-server/lib/handlers/authenticate-handler';

class AuthenticateHandler extends BaseHandler {
  async handle(request: any, response: any) {
    const token = await this.getTokenFromRequest(request);
    const authToken = await this.getAccessToken(token);

    if (!authToken.OAuthClient && authToken.accessTokenExpiresAt < new Date()) {
      throw new InvalidTokenError('Invalid token: access token has expired');
    }

    if (this.scope) {
      await this.verifyScope(authToken);
    }
    this.updateResponse(response, token);

    return authToken;
  }
}

export default AuthenticateHandler;
