import 'mocha';
import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import assertArrays from 'chai-arrays';
import app from '../../../server/services/api/api';
import Story from '../../../model/stories/story';
import Tag from '../../../model/tag';
import {
  stories,
  expectedTagsArray,
  tags,
  tagWithImage,
  dummyTag,
} from './tags.data';

chai.use(chaiHttp);
chai.use(assertArrays);


describe('REST:Tags', () => {
  describe('/tags/all', () => {
    describe('filled database', () => {
      before((done) => {
        Story.deleteMany({}, (err) => {
          Story.insertMany(stories, (err) => { done(); })
        });
      });

      it('it should GET all tags', (done) => {
        chai.request(app)
          .get('/tags/all')
          .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body).to.be.an('array');
            expect(res.body).to.be.equalTo(expectedTagsArray);
            done();
          });
      });
    });

    describe('empty database', () => {
      before((done) => {
        Story.deleteMany({}, (err) => { done(); });
      });

      it('it should GET all tags but get empty array', (done) => {
        chai.request(app)
          .get('/tags/all')
          .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body).to.be.an('array');
            expect(res.body).to.be.ofSize(0);
            done();
          });
      });
    });
  });

  describe('/tags/info/:tag', () => {
    before((done) => {
      Tag.deleteMany({}, (err) => {
        Tag.insertMany(tags, (err) => {
          done();
        })
      });
    });

    it('it should GET tag info', (done) => {
      chai.request(app)
        .get(`/tags/info/${tagWithImage.name}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body).to.deep.equal(tagWithImage);
          expect(res.body).to.not.have.property('__v');
          expect(res.body).to.not.have.property('__id');
          done();
        });
    });

    it('it should GET tag info without image key', (done) => {
      chai.request(app)
        .get(`/tags/info/${dummyTag.name}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body).to.deep.equal(dummyTag);
          expect(res.body).to.not.have.property('image');
          expect(res.body).to.not.have.property('__v');
          expect(res.body).to.not.have.property('__id');
          done();
        });
    });

    it('it should GET 404 when request tag info with empty request param', (done) => {
      chai.request(app)
        .get(`/tags/info`)
        .end((err, res) => {
          expect(res).to.have.status(404);
          done();
        });
    });
  });

});
