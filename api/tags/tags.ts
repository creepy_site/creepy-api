/*
  @swagger
   components:
     schemas:
       Book:
         type: object
         required:
           - title
           - author
           - finished
         properties:
           id:
             type: integer
             description: The auto-generated id of the book.
           title:
             type: string
             description: The title of your book.
           author:
             type: string
             description: Who wrote the book?
           finished:
             type: boolean
             description: Have you finished reading it?
           createdAt:
             type: string
             format: date
             description: The date of the record creation.
         example:
            title: The Pragmatic Programmer
            author: Andy Hunt / Dave Thomas
            finished: true
 */

import express from 'express';
import {
  getTagInfo,
  getAllTagsByStoryStatus,
} from '../../db/tags/tags';

const router = express.Router();
const TAGS_API = '/tags';

/*
 * GET /api/tags/info/:tag
 * Returns tag info
 */
router.route(`${TAGS_API}/info/:tag`).get(async (req, res) => {
  const requestTag = req.params.tag || '';
  const info = await getTagInfo(requestTag);

  console.log(info);

  res.status(info ? 200 : 404).send(info);
});

/*
 * GET /api/tags
 * Returns latest tags.
 */
router.route(`${TAGS_API}/all`).get(async (req, res) => {
  const tags = await getAllTagsByStoryStatus();

  res.status(200).send(tags);
});

export default router;
