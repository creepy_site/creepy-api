import express from 'express';
import sa from 'superagent';

const router = express.Router();
const RECAPTCHA_API = '/recaptcha';

/*
 * GET /api/recaptcha/verify
 * Verify users recaptcha
 */
router.route(`${RECAPTCHA_API}/verify`).post((req, res) => {
  if (!req.body.response) {
    return res.sendStatus(400);
  }

  // TODO: move to external constants
  const SECRET = '6LesOxcUAAAAAK4LibjZKwvB4_9sEPOp32ta1voq';
  const URL = 'https://www.google.com/recaptcha/api/siteverify';

  const body = {
    secret: SECRET,
    response: req.body.response,
  };

  sa.post(URL)
    .type('form')
    .accept('json')
    .send(body)
    .end((error, result) => {
      res.send({ success: result.body.success });
    });
});

export default router;
