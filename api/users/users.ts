import express from 'express';
import { getUserInfo } from '../../db/users/users';
// import { getStorisByUser } from '../../db/stories/stories';

const router = express.Router();
const USERS_API = '/users';

interface Like {
  count: number
}

/*
 * GET /api/users/:user
 * Returns user account info
 */
router.route(`${USERS_API}/:user`).get(async (req, res) => {
  const user = req.params.user || '';
  const accountInfo: any = await getUserInfo(user, true);
  if (!accountInfo || accountInfo.hidden) {
    return res.sendStatus(404);
  }

  // TODO: do smth with likes
  // const storiesList = await getStorisByUser(accountInfo._id);
  const likes: Array<Like> = [];
  const rating = likes.reduce((acc, item) => acc + item.count, 0);

  res.status(200).send({
    ...accountInfo,
    rating,
  });
});

export default router;
