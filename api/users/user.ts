import express from 'express';
import Busboy from 'busboy';
import path from 'path';
import { v4 } from 'uuid';
import fs from 'fs';

import config from '@config';
import { updateUser } from '../../db/users/users';
import auth from '../oauth/authenticate';

const router = express.Router();
const USERS_API = '/user';

const { NODE_ENV } = process.env;

router.route(`${USERS_API}/upload`).post(auth(), (req, res) => {
  let filePath: string = null;
  const busboy = new Busboy({
    headers: req.headers,
    limits: {
      fileSize: 1 * 1024 * 1024,
    },
  });
  const { username, _id } = req.user.User;
  const succefullFinishCallback = async () => {
    await updateUser(_id, { accountImage: filePath });
    res.send(filePath);
  };

  busboy.on('file', (fieldname:any, file:any, filename:any) => {
    // TODO: use https://www.npmjs.com/package/lwip to squize of image
    const outputName = `${username}-${v4()}`;
    filePath = `/data/avatars/${outputName}${path.extname(filename)}`;
    const saveTo = path.join(__dirname, filePath);
    const fileStream = fs.createWriteStream(saveTo);
    file.pipe(fileStream);
    file.on('limit', () => {
      file.unpipe(fileStream);
      res.sendStatus(413);
      // Drop the file
      fileStream.end(() => fs.unlinkSync(saveTo));
    });
  });

  busboy.on('finish', succefullFinishCallback);

  return req.pipe(busboy);
});

export default router;
