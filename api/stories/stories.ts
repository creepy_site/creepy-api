import express from 'express';
import { get, find } from 'lodash';
import auth from '../oauth/authenticate';
import getUser from '../oauth/getUser';

import {
  getPublishedStoryByUID,
  appendCommentsForStory,
  getPublishedStoriesCount,
  findStoryByContent,
  findStoriesByTag,
  getStoriesByUser,
  getRandomStory,
  canUserDoLikeOperation,
  createLikeRecord,
  removeLikeRecord,
  appendUserStory,
  getUserLikesForStoriesList,
  getStoriesList,
} from '../../db/stories/stories';
import { getUserIDbyUsername } from '../../db/users/users';
import STORY_STATUS from '../../dictionary/storyStatus';

const STORY_API = '/stories';
const STORIES_BY_PAGE = 10;

const router = express.Router();

const sendNothing = res => res.sendStatus(204);

/*
* Get stories from database with offset and sorting params, then render its
*/
const getStories = async (sortingLabel, OFFSET, res, next, req) => {
  const { user } = req;
  // It's necessary to check right offset
  const storiesCount = await getPublishedStoriesCount();

  if (OFFSET * STORIES_BY_PAGE >= storiesCount) {
    return res.status(204).send({
      err: true,
      msg: 'stories not found',
    });
  }
  let SKIP_SIZE = OFFSET * STORIES_BY_PAGE;
  // TODO: wtf is 10?
  if (storiesCount > 10 && storiesCount - SKIP_SIZE < 10) {
    SKIP_SIZE = storiesCount - 10;
  }

  const [err, list]: any = await getStoriesList(
    sortingLabel,
    SKIP_SIZE,
    STORIES_BY_PAGE,
    user && user.User._id,
  );

  if (err) {
    res.status(500).send(err);
  }
  res.status(list.length === 0 ? 204 : 200).send(list);
};


/*
 * GET /stories/latest/:offset?
 * Returns latest 10 stories with offset.
 */
router.route(`${STORY_API}/latest/:offset?`).get(getUser(), (req, res, next) => {
  // If offset doent set, it will be 1 and return to 10 latest stories
  const OFFSET = parseInt(req.params.offset, 10) - 1 || 0;

  getStories('datePublished', OFFSET, res, next, req);
});

/*
 * GET /stories/scary/:offset?
 * Returns top rating 10 stories with offset.
 */
router.route(`${STORY_API}/scary/:offset?`).get(getUser(), (req, res, next) => {
  // If offset doent set, it will be 1 and return to 10 latest stories
  const OFFSET = parseInt(req.params.offset, 10) - 1 || 0;

  getStories('likesCount', OFFSET, res, next, req);
});


/*
 * GET /stories/initial
 * Returns users likes by uIDs list
 * auth required
 */
router.route(`${STORY_API}/initial`).post(auth(), async (req, res) => {
  const uids = (req.body.uIDs || []).map(id => +id);
  const userID = req.user.User._id;

  /* Get _ids by users uIDs */
  const [err, list]:any = await getUserLikesForStoriesList(userID, uids);

  if (err) {
    return res.status(403).send(err);
  }

  return res.send(list);
});

/*
 * PUT /stories/new
 * Save user's story in database
 * auth required
 */
router.route(`${STORY_API}/new`).put(auth(), async (req, res) => {
  const {
    content,
    title,
  } = req.body;
  const author = req.user.User._id;

  const err = await appendUserStory({
    title: title.trim(),
    content: content.trim(),
    author,
  });

  res.sendStatus(err ? 500 : 204);
});


/*
 * POST /stories/like/:id
 * Set like to story
 */
router.route(`${STORY_API}/like/:id`).post(auth(), async (req, res) => {
  const userId = req.user.User._id;
  const uID = parseInt(req.params.id, 10) || -1;

  /* Check already exists rating object */
  const isLike = true;
  const operationPermitted = await canUserDoLikeOperation(userId, uID, isLike);

  if (!operationPermitted) {
    return res.status(400).send({
      err: true,
      msg: 'aready set',
    });
  }

  /* Create rating object */
  const err = await createLikeRecord(userId, uID);
  return res.sendStatus(err ? 500 : 204);
});

/*
 * POST /stories/unlike/:id
 * Unset like to story
 */
router.route(`${STORY_API}/unlike/:id`).post(auth(), async (req, res) => {
  const userId = req.user.User._id;
  const uID = parseInt(req.params.id, 10) || -1;

  /* Check already exists rating object */
  const operationPermitted = await canUserDoLikeOperation(userId, uID, false);
  if (!operationPermitted) {
    return res.status(400).send({
      err: true,
      msg: 'doesnt set',
    });
  }

  /* Remove rating object */
  const err = await removeLikeRecord(userId, uID);
  return res.sendStatus(err ? 500 : 204);
});


/*
 * GET /stories/random
 * Return random story
 */
router.route(`${STORY_API}/random`).get(getUser(), async (req, res) => {
  const { user } = req;
  const story: any = await getRandomStory(true);

  if (user) {
    const userId = user.User._id;
    story.wasLiked = !!find(story.likes, { user: userId });
  }
  delete story.likes;

  res.send(story);
});


/*
 * GET /stories/count
 * Return count total stories in database
 */
router.route(`${STORY_API}/count`).get(async (req, res) => {
  const count = await getPublishedStoriesCount();
  res.send({ count });
});


/*
 * GET /stories/tag/:tag
 * Returns strory by tag
 */
router.route(`${STORY_API}/tag/:tag`).get(getUser(), async (req, res) => {
  const { tag = '' } = req.params;
  const { user } = req;

  if (tag.trim() === '') {
    return res.send([]);
  }
  const stories: any = await findStoriesByTag(tag, true);
  if (!stories || stories.length === 0) {
    return res.send([]);
  }

  for (let i = 0; i < stories.length; i += 1) {
    const story = stories[i];

    if (user) {
      const userId = user.User._id;
      story.wasLiked = !!find(story.likes, { user: userId });
    }
    delete story.likes;
    delete story.comments;
  }

  res.send(stories);
});

/*
 * GET /stories/byUser/:username
 * Returns all username's stories
 */
router.route(`${STORY_API}/byUser/:username`).get(getUser(), async (req, res) => {
  const { username = '' } = req.params;
  const currentUser = get(req, 'user.User.username');
  const user: any = await getUserIDbyUsername(username);
  if (!user) {
    return res.send([]);
  }

  const stories: any = await getStoriesByUser(user._id, username === currentUser);

  const resultList = stories.map((story) => {
    const result = {
      uID: story.uID,
      title: story.title,
    } as any;

    // For every published story append likes and comments fields
    if (story.data.status === STORY_STATUS.PUBLISHED) {
      result.likesCount = story.likesCount;
      result.commentsCount = story.commentsCount;
    }

    if (username === user.username) {
      return {
        ...result,
        status: story.data.status,
      };
    }
    return result;
  });

  res.send(resultList);
});


/*
 * GET /stories/search?text=search phrase
 * Returns all matching strory by text query
 */
router.route(`${STORY_API}/search`).get(async (req, res) => {
  let text = req.query.text || '';
  text = (text as String).replace(/\\/g, '');

  if (!text || text === '') {
    res.status(200).send({
      err: true,
      msg: 'text param is missing',
    });
  }

  /* get stories */
  const stories: any = await findStoryByContent(text);
  if (!stories || stories.length === 0) {
    return res.send([]);
  }

  /* calculate likes and comments length for them */
  for (let i = 0; i < stories.length; i += 1) {
    const story = stories[i];

    delete story.likes;
    delete story.comments;
  }

  res.send(stories);
});

/*
 * GET /stories/:id
 * Returns story by id
 */
router.route(`${STORY_API}/:id`).get(getUser(), async (req, res) => {
  const ID = parseInt(req.params.id, 10);
  const { user } = req;

  if (Number.isNaN(ID) || ID <= 0) {
    return res.sendStatus(404);
  }

  /* Get story */
  const storyItem = await getPublishedStoryByUID(ID, user?.User._id);
  if (!storyItem) {
    return sendNothing(res);
  }

  /* Populate by comments */
  const storyWithComments: any = await appendCommentsForStory(storyItem);
  const resultStory = storyWithComments.toJSON();

  /* Check for user like */
  if (user) {
    const userId = user.User._id;
    resultStory.wasLiked = !!find(resultStory.likes, { user: userId });
  }

  delete resultStory._id;
  res.send(resultStory);
});

export default router;
