import express from 'express';

import auth from '../oauth/authenticate';
import Comment from '../../model/stories/comment';
import Story from '../../model/stories/story';

const router = express.Router();
const COMMENTS_API = '/comments/';

// TODO: too complicated function
router.route(COMMENTS_API).put(auth(), (req, res) => {
  const {
    uID,
    content,
  } = req.body;

  const {
    _id: author,
  } = req.user.User;

  if (!content || content.length === 0 || !uID) {
    return res
      .status(400)
      .send({ error: true, msg: 'fill story uID and content' });
  }

  Comment.create({ content, author }, (err, comment) => {
    if (err) {
      return res
        .status(400)
        .send({ error: true, msg: 'internal error' });
    }

    Story.findOneAndUpdate(
      { uID },
      {
        $push: { comments: comment._id },
        $inc: { commentsCount: 1 },
      },
      {},
      (findErr) => {
        if (findErr) {
          return res
            .status(400)
            .send({ error: true, msg: 'internal error' });
        }

        comment.populate({
          path: 'author',
          model: 'User',
          select: 'username -_id',
        }, () => res.status(200).send(comment));
      },
    );
  });
});

router.route(COMMENTS_API).post(auth(), (req, res) => {
  res.status(403).send({
    err: true,
    msg: 'not impliment yet',
  });
});

/*
 * GET /comments
 */
router.route(COMMENTS_API).get(auth(), (req, res) => {
  res.status(403).send({
    err: true,
    msg: 'not impliment yet',
  });
});


export default router;
