import express from 'express';
import getUser from '../oauth/getUser';

const router = express.Router();

const ADMIN_API = '/admin/';


router.route(`${ADMIN_API}/users`).get(getUser('admin'), (req, res) => {
  res.send([{
    mock: true,
  }]);
});


export default router;
