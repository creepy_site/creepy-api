import express from 'express';

import getUser from '../oauth/getUser';
import logger from '../../server/logger';
import Story from '../../model/stories/story';
import getError from '../../common/getError';
import STATUS from '../../dictionary/storyStatus';

const router = express.Router();
const adminAuth = getUser('admin');


const ADMIN_API = '/admin/stories';
const STORY_ID_DB_LABEL = 'uID';
/*
 * GET /admin/index
 * Get stories block info
 */
router.route(`${ADMIN_API}/about`).get(adminAuth, (req, res, next) => {
  Story.aggregate([
    {
      $group: {
        _id: {
          from: '$data.from',
          status: '$data.status',
        },
        byStatus: {
          $push: '$data.status',
        },
        total: {
          $sum: 1,
        },
      },
    }, {
      $group: {
        _id: {
          from: '$_id.from',
        },
        byStatus: {
          $addToSet: {
            statusID: '$_id.status',
            count: '$total',
          },
        },
      },
    },
  ]).exec((err, data) => {
    getError(err, next);
    console.log(JSON.stringify(data));
    let resultData = [];
    // TODO: rewrite function.
    resultData = data.map((dataItem) => {
      const info: any = {
        status: {
          0: 0,
          1: 0,
          2: 0,
          3: 0,
        },
      };
      const { byStatus } = dataItem;
      info.from = dataItem._id.from;

      byStatus.map((item: any) => {
        info.status[item.statusID] = item.count;
        return true;
      });

      return info;
    });

    res.send(resultData);
  });
});

router.route(`${ADMIN_API}/sources`).get(async (req, res) => {
  const result = await Story.distinct('data.from');

  res.send(result);
});

router.route(`${ADMIN_API}/count`).get(async (req, res) => {
  const options = {};
  const { blockName } = req.query;
  if (blockName) {
    options['data.from'] = blockName;
    if (blockName === 'null') {
      options['data.from'] = { $exists: false };
    }
  }

  const count = await Story.countDocuments(options);

  res.status(200).send(count.toString());
});

/*
 * POST /admin/stories/new
 * Create new story
 */
router.route(`${ADMIN_API}/new`).post((req, res) => {
  let tags = [];
  if (req.body.tags.length > 1) {
    tags = req.body.tags.split(',');
  }

  const story: any = new Story({
    content: req.body.content,
    data: {
      // domain
      link: req.body.url,
      // original source link
      src: req.body.src,
      title: req.body.title,
      description: req.body.description,
      rating: parseInt(req.body.rating, 10),
      status: STATUS.NEW,
      simularity: 0,
    },
    tags,
  });

  story.save((err: any) => {
    if (err) {
      return res.send({
        error: true,
        msg: 'save content problem',
      });
    }

    return res.send({ uID: story.uID });
  });
});

/*
 * GET /admin/stories/:id
 * Change status of story
 */
const setStatus = (ID: any, status: any, res: any, next: any): void => {
  const sendNothing = () => res.status(200).send({
    err: true,
    msg: 'story not found',
  });

  if (Number.isNaN(ID) || ID <= 0) {
    sendNothing();
    return null;
  }


  Story.findOneAndUpdate({ [STORY_ID_DB_LABEL]: ID }, {
    $set: {
      'data.status': status,
    },
  }, {
    new: true,
  }, (err, story) => {
    getError(err, next);
    if (!story) {
      return sendNothing();
    }

    res.send(story);
  });
};

router.route(`${ADMIN_API}/:id/apply`).post((req, res, next) => {
  const ID = parseInt(req.params.id, 10);

  setStatus(ID, STATUS.APPLYIED, res, next);
});

router.route(`${ADMIN_API}/:id/reject`).post((req, res, next) => {
  const ID = parseInt(req.params.id, 10);

  setStatus(ID, STATUS.REJECTED, res, next);
});

router.route(`${ADMIN_API}/:id/publish`).post((req, res, next) => {
  const ID = parseInt(req.params.id, 10);
  setStatus(ID, STATUS.PUBLISHED, res, next);

  // When status changed to published at first time
  // Set publish date to story
  Story.findOneAndUpdate({
    [STORY_ID_DB_LABEL]: ID,
    datePublished: {
      $eq: null
    },
  }, {
    $set: {
      datePublished: Date.now(),
    },
  }, {
    new: true,
  }, (err) => {
    if (err) {
      logger.error('Error while change status to published');
    }
  });
});

router.route(`${ADMIN_API}/:id/clearstatus`).post((req, res, next) => {
  const ID = parseInt(req.params.id, 10);

  setStatus(ID, STATUS.NEW, res, next);
});

/*
 * GET /admin/stories/:id
 * Returns strory by id
 */
router.route(`${ADMIN_API}/:id`).get(getUser('admin'), (req, res, next) => {
  const ID = parseInt(req.params.id, 10);

  const sendNothing = () => res.status(200).send({
    err: true,
    msg: 'story not found',
  });


  if (Number.isNaN(ID) || ID <= 0) {
    return sendNothing();
  }

  Story.findOne({
    [STORY_ID_DB_LABEL]: ID,
  }, (err, story) => {
    getError(err, next);
    if (!story) {
      return sendNothing();
    }
    res.send(story);
  });
});

/*
 * POST /admin/stories/:id
 * Updated story data
 */
router.route(`${ADMIN_API}/:id`).post(getUser('admin'), (req, res, next) => {
  const uID = parseInt(req.params.id, 10);

  const resCallback = (err: any) => {
    getError(err, next);
    res.sendStatus(204);
  };

  Story.findOneAndUpdate(
    { uID }, {
      $set: {
        content: req.body.content,
        'data.title': req.body.title,
        'data.description': req.body.description,
        'data.from': req.body.from,
        'data.link': req.body.link,
        tags: req.body.tags,
      },
    },
    {},
    resCallback,
  );
});

/*
 * GET admin/stories?block=name&page=number
 * Get full stories list for admin
 */
router.route(`${ADMIN_API}/`).get((req, res, next) => {
  const STORIES_BY_PAGE = 10;

  const {
    block,
    page,
  } = req.query;

  const OFFSET = Math.max(parseInt(page.toString(), 10) - 1, 0) || 0;
  interface QueryOps {
    'data.from'?: any,
  };

  const queryOpts: QueryOps = {};
  if (block) {
    queryOpts['data.from'] = (block === 'null' ? { $exists: false } : block);
  }

  const SKIP_SIZE = OFFSET * STORIES_BY_PAGE;
  Story.find(queryOpts)
    .skip(SKIP_SIZE)
    .limit(STORIES_BY_PAGE)
    .sort({ 'dateCreated': -1 })
    .populate({
      path: 'author',
      model: 'User',
    })
    .exec((err, stories) => {
      if (err) {
        logger.error(err);
        return res.status(400).send(err);
      }
      getError(err, next);
      if (stories && stories.length > 0) {
        return res.send(stories);
      }

      return res.status(204).send({
        error: true,
        msg: 'EO_STORIES_LIST',
      });
    });
});

export default router;
