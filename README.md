# creepy-api
API server for creepy project

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/942efb7772cd4a32a3f28bbaeae9875e)](https://app.codacy.com/app/Frozzy6/creepy-api?utm_source=github.com&utm_medium=referral&utm_content=Frozzy6/creepy-api&utm_campaign=Badge_Grade_Dashboard)

Setup
```
npm install
npm run dev
```

After you opened [http://127.0.0.1:3001/](http://127.0.0.1:3001/) you will see API description.

# Source Code Style

We use [airbnb](https://github.com/airbnb/javascript)
