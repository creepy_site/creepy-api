const path = require('path');
const nodeExternals = require('webpack-node-externals');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

const {
  NODE_ENV = 'production',
} = process.env;

module.exports = {
  mode: NODE_ENV,
  target: 'node',
  devtool: 'inline-source-map',
  entry: ['@babel/polyfill', './server/server.ts'],
  output: {
    path: path.resolve(__dirname, 'build/'),
    filename: 'server.js',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx'],
    plugins: [new TsconfigPathsPlugin({
      logLevel: 'info'
    })]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          },
        },
      },
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'ts-loader',
          options: {
            projectReferences: true,
          }
        }
      },
    ],
  },
  node: {
    global: false,
    __dirname: true,
  },
  externals: [nodeExternals()],
};
