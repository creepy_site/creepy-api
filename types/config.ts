import { ConnectionOptions } from 'mongoose';

export type ServiceType = "swagger" | "api";

export interface Part {
  database: {
      uri: string;
      options: ConnectionOptions;
  }
  services: {
    [key in ServiceType]: boolean;
  }
  API_HOST: string;
  HOST: string;
};

export interface Config {
  [key: string]: Part;
}
