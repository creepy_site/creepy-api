import { AbstractGrantType } from 'oauth2-server';

declare module "oauth2-server" {
  export interface AbstractGrantType {
    model: any;
    generateAccessToken(args: any): any;
  }
}
