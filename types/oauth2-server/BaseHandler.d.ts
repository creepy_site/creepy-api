declare module 'oauth2-server/lib/handlers/authenticate-handler' {
  class BaseHandler {
    constructor(args: any);
    scope: any;
    getTokenFromRequest(request: any): any;
    getAccessToken(token: any): any;
    verifyScope(authToken: any): void;
    updateResponse(response: any, token: any): void;
  }

  export = BaseHandler
}