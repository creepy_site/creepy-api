/* eslint-disable-next-line */
export const trim = (s: string) => s.replace(/^\s+|\s+$/gm, '');
