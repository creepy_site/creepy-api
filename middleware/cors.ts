import { Request, Response } from 'express';

export default function (req: Request, res: Response, next: () => void) {
  /* https://stackoverflow.com/a/44093028/3310349 */
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type,Authorization');

  // Special for axios from browser
  // First request from browser is OPTIONS to check CORS
  // Sencond request is required method
  if (req.method === 'OPTIONS') {
    res.sendStatus(200);
  } else {
    next();
  }
}
