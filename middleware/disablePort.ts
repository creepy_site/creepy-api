import { Request, Response } from 'express';

const { NODE_ENV } = process.env;

export default function (req: Request, res: Response, next: () => void) {
  const HOST = req.headers.host;

  if (NODE_ENV === 'production' && HOST.match(':3000')) {
    return res.sendStatus(403);
  }
  next();
}
