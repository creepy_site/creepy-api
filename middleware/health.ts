import { Request, Response } from 'express';
import { Mongoose } from 'mongoose';

export default function (mongoose: Mongoose) {
  return (req: Request, res: Response, next: () => void) => {
    // TODO: move to dict
    // Output
    // 0 = disconnected
    // 1 = connected
    // 2 = connecting
    // 3 = disconnecting
    if (mongoose.connection.readyState === 0) {
      res.status(500).send({
        error: true,
        msg: 'Internal server error',
      });
    } else {
      next();
    }
  };
}
