which ssh-agent || ( apk update && apk add openssh-client git )
eval $(ssh-agent -s)
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
mkdir -p ~/.ssh
chmod 700 ~/.ssh
ssh-keyscan -H $VDS_SERVER >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts