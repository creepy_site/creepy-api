import express from 'express';
import swaggerUi from 'swagger-ui-express';
import swaggerJsdoc from 'swagger-jsdoc';

import config from '@config';

const NODE_ENV = process.env['NODE_ENV'];
const cfg = config[NODE_ENV];

const swaggerApplication = express();

const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Scary-stories API",
      version: "0.0.1",
    },
    servers: [
      {
        url: cfg.API_HOST,
      },
    ],
  },
  apis: [
    "../../api/*",
  ],
};

const specs = swaggerJsdoc(options);
console.log(specs);

swaggerApplication.use(
  "/",
  swaggerUi.serve,
  swaggerUi.setup(specs, { explorer: true })
);


export default swaggerApplication;
