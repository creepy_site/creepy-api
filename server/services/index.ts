import api from './api/api';
import swagger from './swagger/swagger';

export default {
  api,
  swagger,
}


