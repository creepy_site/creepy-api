import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import logger from 'morgan';

import * as routes from '../../../api/index';
import corsMiddleware from '../../../middleware/cors';

const apiApplication = express();

apiApplication.use(bodyParser.json());
apiApplication.use(bodyParser.urlencoded({ extended: true }));
apiApplication.use(compression());
apiApplication.use(logger('dev'));
apiApplication.use(corsMiddleware);

Object.keys(routes).map(item => apiApplication.use('/', (routes as any)[item]));

export default apiApplication;
