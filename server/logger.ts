import log4js from 'log4js';

log4js.configure({
  appenders: {
    out: {
      type: 'stdout',
      layout: {
        type: 'pattern',
        pattern: '%[%f{1}:%l %p %d{yyyy/MM/dd-hh:mm:ss}%] %m',
      },
    },
  },
  categories: {
    default: {
      appenders: ['out'],
      level: 'trace',
      enableCallStack: true,
    },
  },
});

const logger = log4js.getLogger();

export default logger;
