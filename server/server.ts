import express from 'express';
import compression from 'compression';
import mongoose from 'mongoose';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import path from 'path';

import config from '@config';
import logger from './logger';

import services from './services';

import disablePortMiddleware from '../middleware/disablePort';
import corsMiddleware from '../middleware/cors';
import notFoundMiddleware from '../middleware/NotFound';
import healthMiddleware from '../middleware/health';

const NODE_ENV = process.env["NODE_ENV"];

console.log(`NODE_ENV: "${NODE_ENV}"`);

const app = express();
const cfg = config[NODE_ENV];

mongoose.connect(cfg.database.uri, cfg.database.options);
mongoose.connection.on('error', () => {
  logger.error('Error: Could not connect to MongoDB. Did you forget to run `mongod`?');
});
mongoose.connection.on('disconnected', () => {
  logger.info('Mongoose has disconnected');
  logger.info('Next auth attempt after 5 sec');
  setTimeout(() => {
    mongoose.connect(cfg.database.uri, cfg.database.options);
  }, 2000);
});

// app.use(express.static(path.join(__dirname, '../public')));

Object.keys(services).forEach((serviceKey) => {
  const service = (services as any)[serviceKey];

  if (cfg.services[serviceKey]) {
    logger.info(`Starting: ${serviceKey}`);
    app.use('/', service);
  } else {
    logger.warn(`Skip: ${serviceKey}`);
  }
});

logger.info(`Server started as: "${process.env.NODE_ENV}"`);

// TODO: move to middleware
// app.use((req, res, next) => {
//   const HOST = req.headers.host;

//   if (NODE_ENV === 'production' && HOST.match(':3001')) {
//     res.sendStatus(404);
//     return false;
//   }
//   next();
// });

app.set('port', process.env.PORT || 3001);
app.enable('trust proxy');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(compression());
app.use(morgan('dev'));

app.use('/data', express.static(path.join(__dirname, '../', 'data')));

app.use(corsMiddleware);
app.use(disablePortMiddleware);
app.use(healthMiddleware(mongoose));
app.use(notFoundMiddleware);

const server = require('http').createServer(app);

server.listen(app.get('port'), () => {
  logger.info(`Express server listening on port ${app.get('port')}`);
});

// process.on('unhandledException', (err) => {
//   throw new Error(err);
// });
