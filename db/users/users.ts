import mongoose from 'mongoose';
import User from '../../model/oauth/User';

export function getUserInfo(username = '', lean = false) {
  const query = User
    .findOne({ username })
    .select({ __v: 0, password: 0 })
    .populate({
      path: 'scope',
      select: '-_id scope',
    })
    .lean(true)

  return new Promise((resolve) => {
    query.exec((err, data) => {
      resolve(data);
    });
  });
}

export function getUserIDbyUsername(username: string) {
  const query = User
    .findOne({ username })
    .select({ _id: 1, username: 1 });

  return new Promise((resolve) => {
    query.exec((err, data) => {
      resolve(data);
    });
  });
}

export function updateUser(id: mongoose.Types.ObjectId, updateObject: any, lean: any = false) {
  console.log('removed lean. check it');
  const query = User
    .findOneAndUpdate(
      { _id: id },
      updateObject,
      // { lean }
    );

  return new Promise((resolve) => {
    query.exec((err: any, data: any) => {
      resolve(data);
    });
  });
}
