import Story from '../../model/stories/story';
import Tag from '../../model/tag';
import STATUS from '../../dictionary/storyStatus';

export function getTagInfo(name: any = '') {
  // Select all fields except "_id"
  const query = Tag
    .findOne({ name })
    .select({ _id: 0, __v: 0 });

  return new Promise((resolve) => {
    query.exec((err, data) => {
      resolve(data);
    });
  });
}

export function getAllTagsByStoryStatus(status = STATUS.PUBLISHED) {
  const query = Story
    .find({ 'data.status': status })
    .distinct('tags');

  return new Promise((resolve) => {
    query.exec((err, list) => {
      resolve(!list ? [] : list);
    });
  });
}

export function createTag(name: string, description = '') {
  if (!name) {
    return false;
  }
  /* TODO: check for already exists */
  return new Promise((resolve) => {
    Tag.create({
      name,
      description,
    }, (err: any, tag: any) => {
      if (err) {
        return resolve(null);
      }
      return resolve(tag);
    });
  });
}

export function getAllTags() {
  return new Promise((resolve) => {
    Tag.find({}, (err, tags) => {
      if (err) {
        return resolve(null);
      }
      return resolve(tags);
    });
  });
}
