export const stories = [
  {
    "uID" : 1,
    "content" : "<p>test</p>",
    "data" : {
      "simularity" : 0,
      "status" : 3,
      "rating" : 1,
      "description" : "desc",
      "title" : "title",
      "src" : "legacy",
      "link" : "http://dwadwa"
    },
    "dateCreated" : new Date(),
    "tags" : [
      "tag1",
      "tag2",
      "tag3"
    ],
    "__v" : 0,
    "datePublished" : new Date(),
  },
  {
    "uID" : 2,
    "content" : "<p>test2</p>",
    "data" : {
      "simularity" : 0,
      "status" : 3,
      "rating" : 2,
      "description" : "desc",
      "title" : "title",
      "src" : "legacy",
      "link" : "http://dwadwa"
    },
    "dateCreated" : new Date(),
    "tags" : [
      "tag4",
      "tag5",
      "tag6"
    ],
    "__v" : 0,
    "datePublished" : new Date(),
  }
];

export const expectedTagsArray = [
  "tag1",
  "tag2",
  "tag3",
  "tag4",
  "tag5",
  "tag6",
];

export const dummyTag = {
  name: 'tag1',
  description: 'tag1 descr',
};

export const tagWithImage = {
  name: 'tag2',
  description: 'tag2 descr',
  image: '/image.jpg',
};

export const tags = [ dummyTag, tagWithImage ];
