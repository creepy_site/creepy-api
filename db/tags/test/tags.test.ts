import 'mocha';
import { expect } from 'chai';
import Story from '../../../model/stories/story';
import Tag from '../../../model/tag';
import {
  getTagInfo,
  getAllTagsByStoryStatus,
  createTag,
  getAllTags,
} from '../tags';
import {
  stories,
  expectedTagsArray,
  tags,
  tagWithImage,
  dummyTag,
} from './tags.data';

describe('DB: Tags', () => {
  describe('getTagInfo', () => {
    before((done) => {
      Tag.deleteMany({}, (err) => {
        Tag.insertMany(tags, (err) => { done();})
      });
    });

    it('noraml flow', async () => {
      const tagInfo = await getTagInfo(dummyTag.name);

      expect(tagInfo).to.be.a('object');
      expect(tagInfo).to.include(dummyTag);
    });

    it('should return tag body with image', async () => {
      const tagInfo = await getTagInfo(tagWithImage.name);

      expect(tagInfo).to.be.a('object');
      expect(tagInfo).to.include(tagWithImage);
    });

    it('should return null', async () => {
      const tagInfo = await getTagInfo(-1);

      expect(tagInfo).to.be.null;
    });
  });


  describe('createTag', () => {
    it('normal flow', async () => {
      const tag = await createTag('a', 'b');

      expect(tag).to.be.a('object');
      expect(tag).to.include({name: 'a', description: 'b'})
    });

    it.skip('with already created tag', async () => {
      const tag = await createTag('a');

      expect(tag).to.be.a('object');
      expect(tag).to.include({name: 'a', description: ''})
    });

    it('with empty description', async () => {
      const tag = await createTag('z');

      expect(tag).to.be.a('object');
      expect(tag).to.include({name: 'z', description: ''})
    });
  });

  describe('getAllTagsByStoryStatus',()=>{
    it.skip('with PUBLISHED status', async () => {
      const tags = await getAllTagsByStoryStatus();
      expect(tags).to.be.a('array');
    });

    it.skip('with status NEW (contains 0 stories)', async () => {
      const tags = await getAllTagsByStoryStatus();
      expect(tags).to.be.a('array');
    });

    it.skip('with wrong status', async () => {
      const tags = await getAllTagsByStoryStatus();
      expect(tags).to.be.a('array');
    });
  })

  describe('getAllTags',()=>{
    it.skip('normal flow', async () => {
      const tags = await getAllTagsByStoryStatus();
      expect(tags).to.be.a('array');
    });

    it.skip('with empty database', async () => {
      const tags = await getAllTagsByStoryStatus();
      expect(tags).to.be.a('array');
    });
  });
});
