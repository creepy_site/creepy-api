import { find, update } from 'lodash';

import Story from '../../model/stories/story';
import logger from '../../server/logger';
import STATUS from '../../dictionary/storyStatus';

export function getStoryByUID(uID: any) {
  if (!uID) {
    return logger.warn('call getStoryByUID without uID param');
  }
}

export function getStoriesList(sortingLabel: any, skipSize: any, limitSize: any, user: any) {
  const query = Story.find({ 'data.status': STATUS.PUBLISHED })
    // (SIC!)
    .sort({ [sortingLabel]: -1 })
    .skip(skipSize)
    .limit(limitSize)
    .select('-__v -data.src -data.simularity -comments')
    .populate({
      path: 'author',
      model: 'User',
      select: '-_id username',
    })
    .lean(true)

  return new Promise((resolve) => {
    query.exec((err, stories: Array<any> = []) => {
      let resultList = stories;
      if (user) {
        /* Append likes */
        resultList = stories.map(story =>
          update(
            story,
            'wasLiked',
            () => !!find(story.likes, { user }),
          ));
      }
      resolve([err, resultList]);
    });
  });
}

export function getPublishedStoryByUID(uID: any, userId) {
  if (!uID) {
    return logger.warn('call getPublishedStoryByUID without uID param');
  }

  const query = Story.findOne({
    uID,
    $or: [
      { 'data.status': STATUS.PUBLISHED },
      { author: userId }
    ]
  }, {
    __v: false,
    'data.src': false,
    'data.simularity': false,
    // 'data.status': true,
    'data.link': false,
  }).populate({
    path: 'author',
    model: 'User',
    select: '_id username',
  });

  return new Promise((resolve) => {
    query.exec((err, story) => {
      resolve(err || story);
    });
  });
}

export function appendCommentsForStory(story: any) {
  return Story.populate(story, {
    path: 'comments',
    select: '-_id -__v',
    populate: {
      path: 'author',
      model: 'User',
      select: 'username accountImage -_id',
    },
  });
}

export function getPublishedStoriesCount() {
  return Story.count({ 'data.status': STATUS.PUBLISHED });
}

export function findStoryByContent(searchPhrase: any) {
  const query = Story
    .find({
      content: new RegExp(searchPhrase, 'i'),
      'data.status': STATUS.PUBLISHED,
    })
    .select({
      __v: 0,
      'data.src': 0,
      'data.simularity': 0,
    });

  return new Promise((resolve) => {
    query.exec((err, list) => resolve(err || list));
  });
}

export async function getRandomStory(lean: any) {
  const storiesCount = await getPublishedStoriesCount();
  const rnd = Math.round(Math.random() * (storiesCount - 1));

  const query = Story.findOne({ 'data.status': STATUS.PUBLISHED }, {
    __v: false,
    'data.src': false,
    'data.simularity': false,
    'data.status': false,
    'data.link': false,
  })
    .skip(rnd)
    .limit(1)
    .populate({
      path: 'comments',
      select: '-_id -__v',
      populate: {
        path: 'author',
        model: 'User',
        select: 'uID username -_id',
      },
    })
    .populate({
      path: 'author',
      model: 'User',
      select: 'uID username -_id',
    })
    .lean(lean);

  return new Promise((resolve) => {
    query.exec((err, res) => resolve(err || res));
  });
}

export function findStoriesByTag(tag: any, lean: any) {
  const query = Story
    .find({
      tags: tag,
      'data.status': STATUS.PUBLISHED,
    })
    .select({
      __v: 0,
      'data.src': 0,
      'data.simularity': 0,
    })
    .populate({
      path: 'author',
      model: 'User',
      select: 'uID username -_id',
    })
    .lean(true)

  return new Promise((resolve) => {
    query.exec((err, list) => resolve(err || list));
  });
}


export function getStoriesByUser(author: any, isNeedAll: any) {
  const query = Story
    .find(isNeedAll ? { author } : {
      'data.status': STATUS.PUBLISHED,
      author,
    })
    .select({
      'data.title': 1,
      'data.status': 1,
      uID: 1,
      _id: 1,
      likesCount: 1,
      commentsCount: 1,
      title: 1,
    });

  return new Promise((resolve) => {
    query.exec((err, list) => resolve(err || list));
  });
}

export function canUserDoLikeOperation(user: any, uID: any, isLike: any) {
  const query = Story.count({ uID, likes: { $elemMatch: { user } } });

  return new Promise((resolve) => {
    query.exec((err, count) => resolve(isLike ? count === 0 : count > 0));
  });
}

export function createLikeRecord(user: any, uID: any) {
  const query = Story.findOneAndUpdate({ uID }, {
    $push: { likes: { user } },
    $inc: { likesCount: 1 },
  });

  return new Promise((resolve) => {
    query.exec(err => resolve(err));
  });
}

export function removeLikeRecord(user: any, uID: any) {
  const query = Story.findOneAndUpdate({ uID }, {
    $pull: { likes: { user } },
    $inc: { likesCount: -1 },
  });

  return new Promise((resolve) => {
    query.exec(err => resolve(err));
  });
}

export function appendUserStory({ title , content, author }: any) {
  return new Promise((resolve) => {
    Story.create({
      content,
      data: {
        from: 'user',
        rating: 0,
        status: STATUS.NEW,
        simularity: 0,
      },
      title,
      author,
    }, (err: any) => resolve(err));
  });
}

export function getUserLikesForStoriesList(user: any, uidsList: any) {
  const query = Story.find({ uID: { $in: uidsList } }, '_id uID likes');

  return new Promise((resolve) => {
    query.exec((err, items?: Array<any>) => {
      if (!items) {
        return resolve([err, []]);
      }

      return resolve([err, items.map(item => ({
        uID: item.uID,
        isLiked: !!find(item.likes, { user }),
      }))]);
    });
  });
}
