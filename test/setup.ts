import 'mocha';
import os from 'os';
import mongoose, { ConnectionOptions } from 'mongoose';
import prepare from 'mocha-prepare';

import { MongoMemoryServer } from 'mongodb-memory-server';

const { NODE_ENV } = process.env;
if (!NODE_ENV) {
  process.env.NODE_ENV = 'development';
}

let mongoServer: MongoMemoryServer;

prepare((done) => {
  console.log(`Platform: ${os.platform()}`);
  mongoServer = new MongoMemoryServer();
  const options: ConnectionOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  };

  mongoServer.getConnectionString().then((mongoUri: string) => {
    mongoose.connect(mongoUri, options);

    mongoose.connection.on('error', (error) => {
      console.log('Error due connection');
      console.log(error);
    });

    mongoose.connection.once('open', () => {
      console.log(`MongoDB successfully connected to ${mongoUri}`);
      done();
    });

  });
}, async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
});
