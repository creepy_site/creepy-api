import mongoose from 'mongoose'; // https://github.com/babel/babel/issues/6915

const { Schema } = mongoose;


const storyLikeSchema = new Schema({
  dateCreated: { type: Date, default: Date.now },
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  story: { type: Schema.Types.ObjectId, ref: 'Story' },
}, {
  collection: 'likes',
});


export default mongoose.model('StoryLike', storyLikeSchema);
