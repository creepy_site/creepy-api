import mongoose from 'mongoose'; // https://github.com/babel/babel/issues/6915

const { Schema } = mongoose;

const commentSchema = new Schema({
  content: String,
  dateCreated: { type: Date, default: Date.now },
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  likes: [{ type: Schema.Types.ObjectId, ref: 'Like' }],
  likesCount: { type: Number, default: 0 },
}, {
  collection: 'comments',
});


export default mongoose.model('Comment', commentSchema);
