import mongoose from 'mongoose'; // https://github.com/babel/babel/issues/6915
import counter from '../plugins/counter';

const { Schema } = mongoose;

const pendingStorySchema = new Schema({
  uID: { type: Number, index: true },
  title: String,
  content: String,
  email: String,
  checked: { type: Boolean, default: false },
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  dateCreated: { type: Date, default: Date.now },
});

pendingStorySchema.plugin(counter, {
  name: 'pending-stories',
  field: 'uID',
});

export default mongoose.model('PendingStory', pendingStorySchema);
