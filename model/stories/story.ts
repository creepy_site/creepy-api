import mongoose from 'mongoose'; // https://github.com/babel/babel/issues/6915
import counter from '../plugins/counter';

const { Schema } = mongoose;

const storySchema = new Schema({
  uID: { type: Number, index: true },
  title: { type: String, default: '' },
  description: { type: String, default: '' },
  content: String,
  data: Schema.Types.Mixed,
  tags: [String],
  comments: [{ type: Schema.Types.ObjectId, ref: 'Comment' }],
  commentsCount: { type: Number, default: 0 },
  views: { type: Number, default: 0 },
  dateCreated: { type: Date, default: Date.now },
  datePublished: { type: Date, default: null },
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  likes: [{
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    datetime: { type: Date, default: Date.now },
  }],
  likesCount: { type: Number, default: 0 },
}, {
  collection: 'stories',
});

storySchema.plugin(counter, { name: 'stories', field: 'uID' });

export default mongoose.model('Story', storySchema);
