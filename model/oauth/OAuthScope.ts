import mongoose from 'mongoose'; // https://github.com/babel/babel/issues/6915

const { Schema } = mongoose;

const OAuthScopeSchema = new Schema({
  scope: String,
  is_default: Boolean,
});

export default mongoose.model('OAuthScope', OAuthScopeSchema);
