import mongoose from 'mongoose'; // https://github.com/babel/babel/issues/6915

const { Schema } = mongoose;

const OAuthClientSchema = new Schema(
  {
    name: String,
    client_id: String,
    client_secret: String,
    scope: [
      {
        type: Schema.Types.ObjectId,
        ref: 'OAuthScope',
      },
    ],
  },
  {
    collection: 'oauthclients',
  },
);

export default mongoose.model('OAuthClient', OAuthClientSchema);
