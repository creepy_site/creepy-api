import mongoose from 'mongoose'; // https://github.com/babel/babel/issues/6915

const { Schema } = mongoose;

const UserSchema = new Schema({
  username: { type: String, index: true },
  password: String,
  email: String,
  hidden: { type: Boolean, default: false },
  description: String,
  dateRegister: { type: Date, default: Date.now },
  firstPubDate: { type: Date, default: null },
  accountImage: { type: String, default: null },
  scope: [
    {
      type: Schema.Types.ObjectId,
      ref: 'OAuthScope',
    },
  ],
});


export default mongoose.model('User', UserSchema);
