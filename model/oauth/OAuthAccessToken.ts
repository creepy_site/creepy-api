import mongoose from 'mongoose'; // https://github.com/babel/babel/issues/6915

const { Schema } = mongoose;

const OAuthAccessTokenSchema = new Schema({
  access_token: String,
  expires: Date,
  scope: [String],
  type: String,
  User: { type: Schema.Types.ObjectId, ref: 'User' },
  OAuthClient: { type: Schema.Types.ObjectId, ref: 'OAuthClient' },
});

export default mongoose.model('OAuthAccessToken', OAuthAccessTokenSchema);
