import mongoose from 'mongoose';

const tagSchema = new mongoose.Schema({
  name: { type: String, index: true },
  description: String,
  image: String,
});

export default mongoose.model('Tag', tagSchema);
