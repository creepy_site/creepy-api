import mongoose from 'mongoose'; // https://github.com/babel/babel/issues/6915

const { Schema } = mongoose;

const defaultOptions = {
  name: 'generic',
  field: 'counter',
};

export default function (schema: any, options = defaultOptions) {
  const {
    name = 'generic',
    field = 'counter',
  } = options;

  const counterSchema = new Schema({
    name: String,
    counter: Number,
  });

  // TODO: there is need some configuration before plugin will be set
  let CounterModel: any;
  try {
    CounterModel = mongoose.model('counters');
  } catch (e) {
    CounterModel = mongoose.model('counters', counterSchema);
  }

  schema.pre('save', function cb(next: (err?: any) => void) {
    if (!this.isNew) {
      return next();
    }

    CounterModel.findOneAndUpdate(
      { name },
      { $inc: { counter: 1 } },
      { upsert: true },
      (err:any, record:any = { counter: 0 }) => {
        if (err) {
          return next(err);
        }
        this.set(field, record.counter + 1);
        next();
      },
    );
  });
}
